from cgi import parse_qs
def application(environ, start_response):
	lines = []
	lines.append('HELLO WORLD!!')
	lines.append(environ['REQUEST_METHOD'])
	if lines[1] == 'POST':
		d = parse_qs(environ['wsgi.input'].readline().decode())
	else:
		d = parse_qs(environ['QUERY_STRING'])
	for key, value in d.items():
		lines.append("{0}: {1}".format(key, value))
	#for key in environ:
	#	lines.append("{0}: {1}".format(key,environ[key]))
	start_response("200 OK", [("Content-Type", "text/plain")])
	return ["\n".join(lines)]
