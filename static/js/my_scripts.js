$(document).ready(function() {
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }

            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });

    $('.like').on('click', function () {
        var $like = $(this);
        $.post(
            '/like/',
            {id: $like.attr('id')}
        ).done(function (resp) {
                if (resp.error) {
                    $(".main").prepend("<div class=\"alert alert-error\">" +
                        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                        "<strong>Error!</strong> " + resp.error + "</div>")
                }
                else {
                    $(".like_msg_" + $like.attr('id')).prepend("<div class=\"alert alert-success\">" +
                        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                        "<strong>Likes, MORE LIKES! </strong> " + resp.like_msg + "</div>");
                    var response_string = '(' + resp.new_count + ') likes';
                    $like.val(response_string)
                }
            });
    });

    $('.its_right').on('click', function () {
        var $right = $(this);
        $.post(
            '/answer',
            {id: $right.attr('id')}
        ).done(function(resp) {
                $right.remove('.its_right');
                $('.right_answer_'+resp.right_answer).css({'background-color': 'lightgreen'});
            })
    });
});