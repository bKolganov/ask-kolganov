# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0005_like'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name=b'avatar_url',
        ),
        migrations.AddField(
            model_name='profile',
            name='avatar',
            field=models.ImageField(default='no_avatar.jpg', upload_to=b'media'),
            preserve_default=False,
        ),
    ]
