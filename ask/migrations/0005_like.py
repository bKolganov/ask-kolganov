# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0004_answer'),
    ]

    operations = [
        migrations.CreateModel(
            name='Like',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.ForeignKey(to='ask.Profile', to_field=u'id')),
                ('question', models.ForeignKey(to='ask.Question', to_field=u'id')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
