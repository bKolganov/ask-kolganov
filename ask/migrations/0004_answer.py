# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0003_question'),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.ForeignKey(to='ask.Question', to_field=u'id')),
                ('author', models.ForeignKey(to='ask.Profile', to_field=u'id')),
                ('text', models.TextField()),
                ('add_date', models.DateTimeField(auto_now_add=True)),
                ('is_right', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
