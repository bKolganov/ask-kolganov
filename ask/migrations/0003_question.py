# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0002_profile'),
    ]

    operations = [
        migrations.CreateModel(
            name='Question',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('author', models.ForeignKey(to='ask.Profile', to_field=u'id')),
                ('title', models.CharField(max_length=80)),
                ('text', models.TextField()),
                ('add_date', models.DateTimeField(auto_now_add=True)),
                ('tags', models.ManyToManyField(to='ask.Tag')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
