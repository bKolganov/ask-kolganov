from django.forms import ModelForm
from django import forms
from ask.models import Profile, Question, Tag, Answer
from django.contrib.auth.models import User

class UserForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput)
    first_name = forms.CharField(max_length=30, required=False)
    last_name = forms.CharField(max_length=30, required=False)
    email = forms.EmailField()
    avatar = forms.ImageField(required=False)

    def clean_username(self):
        data = self.cleaned_data
        try:
            User.objects.get(username=data['username'])
        except User.DoesNotExist:
            return data['username']
        raise forms.ValidationError('This username is already taken.')

    def clean_email(self):
        data = self.cleaned_data
        try:
            User.objects.get(email=data['email'])
        except User.DoesNotExist:
            return data['email']
        raise forms.ValidationError('This email is already taken.')

    def save(self):
        data = self.cleaned_data
        u = User.objects.create_user(username=data['username'], password=data['password'], email=data['email'],
                                     first_name=data['first_name'], last_name=data['last_name'])
        Profile.objects.create(user_id=u.id, avatar=data['avatar'])

class QuestionNonModelForm(forms.Form):
    title = forms.CharField(max_length=80, required=True)
    text = forms.CharField(widget=forms.Textarea, required=True)
    tags = forms.CharField()

    def save(self, author_id):
        data = self.cleaned_data
        tags_object = []
        for t in data['tags'].split(', '):
            #print()
            tags_object.append(Tag.objects.get_or_create(word=t))
        q = Question.objects.create(title=data['title'], text=data['text'], author_id=author_id)
        for t in tags_object:
            q.tags.add(t[0])
        return q.id

class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['text']
        exclude = ['author_id', 'question_id']

class ProfileEditForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput, required=False)
    first_name = forms.CharField(max_length=30, required=False)
    last_name = forms.CharField(max_length=30, required=False)
    #email = forms.EmailField()
    avatar = forms.ImageField(required=False, widget=forms.FileInput)

    def save(self, profile_id):
        data = self.cleaned_data
        profile = Profile.objects.get(id=profile_id)
        profile.user.first_name = data['first_name']
        profile.user.last_name = data['last_name']
        if data['avatar']:
            profile.avatar = data['avatar']
        if data['password']:
            profile.user.set_password(data['password'])
        profile.user.save()
        profile.save()
