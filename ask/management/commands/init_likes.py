from django.core.management.base import BaseCommand
from ask.models import Profile, Question, Like
from random import randint
from django.db.models import Min, Max
class Command(BaseCommand):

    def handle(self, *args, **options):
        frm_p = Profile.objects.all().aggregate(Min('id'))['id__min']
        to_p = Profile.objects.all().aggregate(Max('id'))['id__max']
        frm_q = Question.objects.all().aggregate(Min('id'))['id__min']
        to_q = Question.objects.all().aggregate(Max('id'))['id__max']
        percent = 0
        print("Likes initialization started")
        for i in range(0, 250000):
            if (i % 2500 == 0):
                print("{}% complete".format(percent))
                percent += 1
            Like.objects.create(user_id=randint(frm_p, to_p), question_id=randint(frm_q, to_q));
        print("Likes initialization ended")