from django.core.management.base import BaseCommand
from ask.models import Tag
from faker import Faker
class Command(BaseCommand):

    def handle(self, *args, **options):
        print("Tags Initialization started")
        fake = Faker()
        for i in range(0, 100):
            print("{}% complete".format(i))
            Tag.objects.create(word=fake.word())
        print("Tags Initialization ended")