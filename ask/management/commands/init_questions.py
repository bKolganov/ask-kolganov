from django.core.management.base import BaseCommand
from ask.models import Question, Profile, Tag
from django.db.models import Min, Max
from random import randint
from faker import Faker
class Command(BaseCommand):

    def handle(self, *args, **options):
        fake = Faker()
        frm = Profile.objects.all().aggregate(Min('id'))['id__min']
        to = Profile.objects.all().aggregate(Max('id'))['id__max']
        t = Tag.objects.order_by('?')
        percent = 0
        print("Questions initialization started")
        for i in range(0, 100000):
            if((i % 1000) == 0):
                print ("{}% complete".format(percent))
                percent += 1
            q = Question.objects.create(title=fake.sentence(nb_words=6,
                                                            variable_nb_words=True)[0:79],
                                                            text=fake.text(max_nb_chars=300),
                                                            author_id=randint(frm, to))
            for x in range(0, 5):
                q.tags.add(t[x])
        print("Questions initialization ended")