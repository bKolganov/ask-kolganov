from django.core.management.base import BaseCommand
from ask.models import Profile
from django.core.cache import caches

memcached = caches['default']
class Command(BaseCommand):
    
   def handle(self, *args, **options):
       memcached.set('users', Profile.top()[:10], 3600)