from django.core.management.base import BaseCommand
from ask.models import Profile
from django.contrib.auth.models import User
from random import randint
from faker import Faker
class Command(BaseCommand):

    def handle(self, *args, **options):
        fake = Faker()
        print("User Initialization started")
        percent = 0
        for i in range(0, 10000):
            if ( i % 100 == 0 ):
                print("{}% complete".format(percent))
                percent += 1
            tmp = fake.simple_profile()
            u = User.objects.create(username=(tmp['username']+str(i)), email=tmp['mail'], first_name=tmp['name'][0:29])
            Profile.objects.create(user_id=u.id, rating=randint(0, 100))
        print("User Initialization ended")