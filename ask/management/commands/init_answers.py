from django.core.management.base import BaseCommand
from ask.models import Profile, Answer, Question
from random import randint
from faker import Faker
from django.db.models import Min, Max
class Command(BaseCommand):

    def handle(self, *args, **options):
        fake = Faker()
        frm_p = Profile.objects.all().aggregate(Min('id'))['id__min']
        to_p = Profile.objects.all().aggregate(Max('id'))['id__max']

        frm_q = Question.objects.all().aggregate(Min('id'))['id__min']
        to_q = Question.objects.all().aggregate(Max('id'))['id__max']
        percent = 0
        print("Answers initialization started")
        for i in range(0, 1000000):
            if (i % 10000 == 0):
                print("{}%".format(percent))
                percent+=1
            q_id = randint(frm_q, to_q)
            p_id = randint(frm_p, to_p)
            Answer.objects.create(author_id=p_id, question_id=q_id,
                                      text=fake.text(max_nb_chars=150))
        print("Answers initialization ended")