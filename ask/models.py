from django.db import models
from django.contrib.auth.models import User
from django.db.models import Count
from djangosphinx.models import SphinxSearch
import uuid
import os.path

def generate_file_name(_, filename):
    print str(uuid.uuid4()) + os.path.splitext(filename)[1]
    return str(uuid.uuid4()) + os.path.splitext(filename)[1]

class Tag(models.Model):
    word = models.CharField(max_length=40)

    @staticmethod
    def top():
        return Tag.objects.annotate(total=Count('question')).order_by('-total')

class Profile(models.Model):
    user = models.OneToOneField(User)
    rating = models.IntegerField(default=0)
    avatar = models.ImageField(upload_to=generate_file_name, default='no_avatar.jpg')

    @staticmethod
    def top():
        return Profile.objects.annotate(total=Count('question')).order_by('-total')

class Question(models.Model):
    author = models.ForeignKey(Profile)
    title = models.CharField(max_length=80)
    text = models.TextField()
    add_date = models.DateTimeField(auto_now_add=True)
    tags = models.ManyToManyField(Tag)
    search = SphinxSearch(index='question_index', weights={'title': 100, 'text': 30})

    def answers(self):
        return Answer.objects.filter(question=self)

    def likes(self):
        return Like.objects.filter(question=self)

    @staticmethod
    def top():
        return Question.objects.annotate(total=Count('like')).order_by('-total')

    @staticmethod
    def latest():
        return Question.objects.order_by("-add_date").all()

class Like(models.Model):
    user = models.ForeignKey(Profile)
    question = models.ForeignKey(Question)

class Answer(models.Model):
    question = models.ForeignKey(Question)
    author = models.ForeignKey(Profile)
    text = models.TextField()
    add_date = models.DateTimeField(auto_now_add=True)
    is_right = models.BooleanField(default=False)


