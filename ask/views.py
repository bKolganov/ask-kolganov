from django.shortcuts import render, redirect
import django.contrib.auth as auth
from models import Question, Profile, Answer, Like
from django.core.paginator import Paginator
from ask.forms import UserForm, QuestionNonModelForm, AnswerForm, ProfileEditForm
from django.contrib.auth.decorators import login_required
import json
from django.http import HttpResponse



def login(request):
    d = {}
    profile = Profile.objects.filter(user_id=request.user.id).first() or ''
    d.update({'profile': profile})
    if request.method == 'GET':
        form = UserForm
        d.update({'form': form})
        return render(request, 'login.html', d)
    else:
        username = request.POST['username']
        password = request.POST['password']
        print username
        print password
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth.login(request, user)
                return redirect(request.POST.get('next') or '/')
        else:
            form = UserForm
            d.update({'error': 'Invalid login', 'form': form})
            return render(request, 'login.html', d)

def signup(request):
    d = {}
    if request.method == 'GET':
        form = UserForm
        d.update({'form': form})
        return render(request, 'signup.html', d)
    else:
        form = UserForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            new_user = auth.authenticate(username=request.POST['username'],
                                         password=request.POST['password'])
            auth.login(request, new_user)
            return redirect(request.POST.get('next') or '/')
        else:
            d.update({'form': form, 'errors': form.errors})
            return render(request, 'signup.html', d)

def logout(request):
    auth.logout(request)
    response = redirect(request.GET.get('next') or '/')
    response.delete_cookie('user_location')
    return response

def top(request):
    d = {}
    profile = Profile.objects.filter(user_id=request.user.id).first() or ''
    page_num = request.GET.get('page', 1)
    paginator = Paginator(Question().top(), 5)
    result = paginator.page(page_num)
    d.update({"result": result, 'profile': profile})
    return render(request, "index.html", d)

def latest(request):
    d = {}
    profile = Profile.objects.filter(user_id=request.user.id).first() or ''
    page_num = request.GET.get('page', 1)
    paginator = Paginator(Question().latest(), 5)
    result = paginator.page(page_num)
    d.update({"result": result, 'profile': profile})
    return render(request, "index.html", d)

def question(request, q_id):
    d = {}
    if request.method == 'GET':
        form = AnswerForm
        profile = Profile.objects.filter(user_id=request.user.id).first() or ''
        page_num = request.GET.get('page', 1)
        question = Question.objects.get(id=q_id)
        paginator = Paginator(Answer.objects.filter(question=question), 5)
        result = paginator.page(page_num)
        d.update({"question": question, "result": result, 'profile': profile, 'form': form})
        return render(request, "question.html", d)
    else:
        form = AnswerForm(request.POST)
        if form.is_valid():
            q = form.save(commit=False)
            q.author_id = Profile.objects.get(user_id=request.user.id).id
            q.question_id = q_id
            q.save()
            return redirect('/question/{}'.format(q_id))
        else:
            return

def index(request):
    d = {}
    profile = Profile.objects.filter(user_id=request.user.id).first() or ''
    page_num = request.GET.get('page', 1)
    paginator = Paginator(Question.objects.all(), 5)
    result = paginator.page(page_num)
    d.update({"result": result, 'profile': profile})
    return render(request, "index.html", d)


@login_required(login_url='login/')
def ask(request):
    d = {}
    profile = Profile.objects.filter(user_id=request.user.id).first() or ''
    d.update({'profile': profile})
    if request.method == 'GET':
        form = QuestionNonModelForm
        d.update({'form': form})
        return render(request, 'asking.html', d)
    else:
        form = QuestionNonModelForm(request.POST)
        if form.is_valid():
            id = form.save(profile.id)
            return redirect('/question/{}'.format(id))
        else:
            d.update({"form": form, "errors": form.errors})
            return render(request, 'asking.html', d)

@login_required(login_url='login/')
def self_profile(request):
    d = {}
    profile = Profile.objects.filter(user_id=request.user.id).first() or ''
    d.update({'profile': profile})
    if request.method == 'GET':
        init_dict = {}
        init_dict['password'] = profile.user.password
        init_dict['first_name'] = profile.user.first_name
        init_dict['last_name'] = profile.user.last_name
        init_dict['email'] = profile.user.email
        init_dict['avatar'] = profile.avatar
        p_form = ProfileEditForm(initial=init_dict)
        d.update({'form': p_form})
        return render(request, 'self_profile.html', d)
    else:
        print(request.POST)
        form = ProfileEditForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(profile.id)
            return redirect('/profile')
        else:
            d.update({"form": form, "errors": form.errors})
            return render(request, 'self_profile.html', d)

def profile(request, p_id):
    d = {}
    v_profile = Profile.objects.get(id=p_id)
    profile = Profile.objects.filter(user_id=request.user.id).first() or ''
    d.update({'profile': profile, 'v_profile': v_profile})
    return render(request, 'profile.html', d)


def like(request):
    if request.method == 'POST':
        response_data = {}
        if not request.user.is_authenticated():
            response_data['error'] = 'you are not login'
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        q_id = request.POST.get('id')
        p_id = Profile.objects.filter(user_id=request.user.id).first().id
        like = Like.objects.filter(user_id=p_id, question_id=q_id)
        if like.exists():
            like.delete()
            response_data['like_msg'] = 'You have removed like'
        else:
            Like.objects.create(user_id=p_id, question_id=q_id)
            response_data['like_msg'] = 'You are awesome!'
        response_data['new_count'] = Question.objects.get(id=q_id).likes().count()
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    return redirect('/')


def answer(request):
    if request.method == 'POST':
        a_id = request.POST.get('id')
        right_answer = Answer.objects.get(id=a_id)
        answers = right_answer.question.answers()
        for a in answers:
            a.is_right = False
            a.save()
        right_answer.is_right = True
        right_answer.save()
        response_data = {}
        response_data['right_answer'] = right_answer.id
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    return redirect('/')

def search(request):
    d = {}
    profile = Profile.objects.filter(user_id=request.user.id).first() or ''
    page_num = request.GET.get('page', 1)
    query = request.GET.get('query')
    paginator = Paginator(Question.search.query(query), 5)
    query_for_template = '&query=' + query
    result = paginator.page(page_num)
    d.update({"result": result, 'profile': profile, 'query': query_for_template})
    return render(request, "index.html", d)