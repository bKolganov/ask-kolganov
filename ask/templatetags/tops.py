from django import template
from django.core.cache import caches

register = template.Library()
memcached = caches['default']

@register.inclusion_tag('addons/users.html')
def top_user():
    return {'users': memcached.get('users')}

@register.inclusion_tag('addons/tags.html')
def top_tag():
    return {'tags': memcached.get('tags'), "rnd_col": ["red", "blue", "green", "yellow", "black", "darkred", "orange", "darkgreen", "pink",
                            "magenta", "lime"]}