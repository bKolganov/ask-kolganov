"""
WSGI config for ask_kolganov project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/dev/howto/deployment/wsgi/
"""

import os,sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ask_kolganov.settings")
sys.path.append('/var/www/ask_kolganov')

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
