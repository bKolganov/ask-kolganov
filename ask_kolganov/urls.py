from django.contrib import admin
from django.conf import settings
from django.conf.urls import include, patterns, url

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ask_kolganov.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'ask.views.index'),
    #url(r'^mywsgi/', 'ask.views.index'),
    url(r'^like', 'ask.views.like'),
    url(r'^answer', 'ask.views.answer'),
    url(r'^question/(?P<q_id>[0-9]+)', 'ask.views.question'),
    url(r'^admin', include(admin.site.urls)),
    url(r'^login', 'ask.views.login'),
    url(r'^logout', 'ask.views.logout'),
    url(r'^signup', 'ask.views.signup'),
    url(r'^latest', 'ask.views.latest'),
    url(r'^top', 'ask.views.top'),
    url(r'^ask', 'ask.views.ask'),
    url(r'^profile/(?P<p_id>[0-9]+)', 'ask.views.profile'),
    url(r'^profile', 'ask.views.self_profile'),
    url(r'^search', 'ask.views.search')
)


if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
